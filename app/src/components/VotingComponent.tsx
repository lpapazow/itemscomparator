import React, {useEffect, useState} from 'react';
import { useHistory } from "react-router-dom";
import { useCookies } from "react-cookie";

export interface ItemsPair {
    imageOne: ImageNameAndPersistedName;
    imageTwo: ImageNameAndPersistedName;
}

export interface ImageNameAndPersistedName {
    name: string;
    persistedName: string
}

const VotingComponent = () => {

    const [itemsPair, setItemsPair] = useState<ItemsPair | undefined>(undefined);
    const [leftImage, setLeftImage] = useState<any>();
    const [rightImage, setRightImage] = useState<any>();
    const [cookies, setCookie, removeCookie] = useCookies(['shownImages']);
    const [totalDistinctItems, setTotalDistinctItems] = useState<number>(0);
    const history = useHistory();
    const goResults = () => history.push('/results');

    useEffect(() => {
        fetchPairToCompare();
        fetchTotalCount();
        setCookie('shownImages', "");
    }, []);

    const fetchPairToCompare = () => {
        fetch('/api/get-two-random-items', {
        })
        .then(response => response.json())
        .then(itemsPair => {
            setItemsPair(itemsPair);
            if (itemsPair.imageOne?.persistedName && itemsPair.imageTwo?.persistedName) {
                setCookie('shownImages', `${cookies['shownImages'] || ''}${itemsPair.imageOne.persistedName};${itemsPair.imageTwo.persistedName};`);
            }
        })
    }

    const fetchTotalCount = () => {
        fetch('/api/all-images-count', {
        })
        .then(response => response.json())
        .then(count => setTotalDistinctItems(count))
    }

    useEffect(() => {
        if (itemsPair && itemsPair.imageOne && itemsPair.imageTwo) {
            fetch(`/api/get-image/${itemsPair.imageOne.persistedName}`, {
            })
            .then((response) => {
                return response.blob();
            })
            .then((blob) => {
                const reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = () => {
                    const base64data = reader.result;
                    setLeftImage(base64data);
                };
            });

            fetch(`/api/get-image/${itemsPair.imageTwo?.persistedName}`, {
            })
            .then((response) => {
                return response.blob();
            })
            .then((blob) => {
                const reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = () => {
                    const base64data = reader.result;
                    setRightImage(base64data);
                };
            });
        }
    }, [itemsPair])

    const vote = (winner: ImageNameAndPersistedName | undefined, loser: ImageNameAndPersistedName | undefined) => {
        if (winner && loser) {
            fetch('/api/vote-for-item', {
                method: 'post',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    "stronger": winner.persistedName,
                    "weaker": loser.persistedName
                })
            })
            .then(() => fetchPairToCompare())
        }
    }

    return (
        <div className="App" style={{fontSize: 30}}>
            <nav className="navbar navbar-dark bg-dark">
                <a className="navbar-brand" href="#">Strongest looking item</a>
            </nav>

            <div className="container">

                <h1 style={{padding: 20, textAlign: "center", fontSize: "9vmin"}}>Which item looks stronger?</h1>

                <div className="row">
                    <div className="col-2">
                    </div>
                    <div className="col-4">
                        <img style={{cursor: 'grab'}} src={leftImage} alt="" onClick={() => vote(itemsPair?.imageOne, itemsPair?.imageTwo)} />
                    </div>
                    <div className="col-4">
                        <img style={{cursor: 'grab'}} src={rightImage} alt="" onClick={() => vote(itemsPair?.imageTwo, itemsPair?.imageOne)} />
                    </div>
                    <div className="col-2">
                    </div>
                </div>
                {totalDistinctItems <= cookies.shownImages?.split(';').length &&
                    <div className="fixed-bottom alert alert-success" role="alert">
                        All different items have been displayed at least once. Rotating previously displayed items.
                    </div>
                }
            </div>
        </div>
    );
}

export default VotingComponent;
