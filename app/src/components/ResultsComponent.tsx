import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";

export interface ItemImageNoByteArray {
    persistedName: string;
    name: string;
    score: number;
}

export interface ItemImageNoByteArray {
    persistedName: string;
    votes: number;
    name: string;
    score: number;
    image: any;
}

interface NameAndImage {
    image: any;
    name: string;
}

const ResultsComponent: React.FC = () => {

    const [orderedImages, setOrderedImages] = useState<ItemImageNoByteArray[]>([]);
    const [nameToImage, setNameToImage] = useState<{[key: string]: any}>({});
    const [currentImage, setCurrentImage] = useState<NameAndImage>();

    useEffect(() => {
        fetch("/api/results")
        .then(response => {
            return response.json();
        })
        .then((images: ItemImageNoByteArray[]) => {
            images.forEach(image => {
                fetch(`/api/get-image/${image.persistedName}`, {
                })
                .then((response) => {
                    return response.blob();
                })
                .then((blob) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(blob);
                    reader.onloadend = () => {
                        const base64data = reader.result;
                        setCurrentImage({ name: image.persistedName, image: base64data});
                    };
                });
            });
            setOrderedImages(images);
        })
    }, []);

    useEffect(() => {
        console.log({...nameToImage});
        if (currentImage) {
            setNameToImage({...nameToImage, [currentImage.name]: currentImage.image});
        }
    }, [currentImage])

    return (
        <div className="App">
            <nav className="navbar navbar-dark bg-dark">
                <a className="navbar-brand" href="#">Strongest looking item</a>
            </nav>
            <div className="container">
                <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">Rank</th>
                        <th scope="col">Item</th>
                        <th scope="col">Total votes</th>
                        <th scope="col">Relative rating
                            <button
                                className="btn btn-info mx-2 py-0"
                                style={{borderRadius: "25px"}}
                                onClick={() => alert("Every item starts with an average rating of 3278 which corresponds to the average gold price of these items in the game. Then the chess-based elo rating system is applied every time a vote is cast reducing the loser's score and increasing the winner's score by the same amount.")}
                            >i</button>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                     {orderedImages.map((image, index) =>
                         <tr key={image.persistedName}>
                             <th scope="row">{index + 1}</th>
                             <td><img src={nameToImage[image.persistedName]}/></td>
                             <td>{image.votes}</td>
                             <td>{Math.round(image.score)}</td>
                         </tr>
                     )}
                    </tbody>
                </table>
            </div>
        </div>


        // <div>
        //     {orderedImages.map(image =>
        //         <div key={image.persistedName}>
        //             <div key={image.persistedName}>{image.score}</div>
        //             <img src={nameToImage[image.persistedName]}/>
        //         </div>
        //         )}
        // </div>
    );
}

export default ResultsComponent;
