import React, {useEffect, useState} from 'react';
import { Route, Switch } from "react-router";
import VotingComponent from "./components/VotingComponent";
import { BrowserRouter as Router } from 'react-router-dom';
import ResultsComponent from "./components/ResultsComponent";

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/" component={VotingComponent} exact />
                <Route path="/results" component={ResultsComponent} exact />
            </Switch>
        </Router>
    )
}

export default App;
