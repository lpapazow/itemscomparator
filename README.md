<h2>How to run it</h2>

<h3> Quick run </h3>

Run `./mvnw spring-boot:run -Pprod `

Open `http://localhost:8080 (also available on http://localhost:8080/index.html)`

<h3> Build and run </h3>

Run `mvn clean install -DskipTests -Pprod`

And in the target folder `java -jar strongestdota2item-0.0.1-SNAPSHOT.jar -Dkey=<admin_password>`

<h2>How to develop: </h2>

0. Install nodejs and yarn.
1. Start `VotecalculatorApplication.java` class
2. In /app/ run `yarn start`

