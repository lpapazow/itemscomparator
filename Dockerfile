FROM adoptopenjdk/openjdk11:ubi
RUN mkdir /opt/app
COPY target/strongestdota2item-0.0.1-SNAPSHOT.jar /opt/app/app.jar
CMD ["nohup", "java", "-jar", "-Dkey=<admin_password", "/opt/app/app.jar", "&"]
