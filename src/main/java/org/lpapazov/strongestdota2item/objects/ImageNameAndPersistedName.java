package org.lpapazov.strongestdota2item.objects;

import java.util.Objects;

public class ImageNameAndPersistedName {
    private String name;
    private String persistedName;

    public String getName() {
        return name;
    }

    public String getPersistedName() {
        return persistedName;
    }

    public ImageNameAndPersistedName(String name, String persistedName) {
        this.name = name;
        this.persistedName = persistedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ImageNameAndPersistedName that = (ImageNameAndPersistedName) o;
        return Objects.equals(name, that.name) && Objects.equals(persistedName, that.persistedName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, persistedName);
    }
}
