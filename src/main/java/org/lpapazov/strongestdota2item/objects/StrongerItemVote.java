package org.lpapazov.strongestdota2item.objects;

public class StrongerItemVote {
    private String stronger;
    private String weaker;

    public String getStronger() {
        return stronger;
    }

    public String getWeaker() {
        return weaker;
    }
}
