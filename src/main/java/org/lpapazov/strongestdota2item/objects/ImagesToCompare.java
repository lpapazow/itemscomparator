package org.lpapazov.strongestdota2item.objects;

public class ImagesToCompare {
    private ImageNameAndPersistedName imageOne;
    private ImageNameAndPersistedName imageTwo;

    public ImagesToCompare(ImageNameAndPersistedName imageOne, ImageNameAndPersistedName imageTwo) {
        this.imageOne = imageOne;
        this.imageTwo = imageTwo;
    }

    public ImageNameAndPersistedName getImageOne() {
        return imageOne;
    }

    public ImageNameAndPersistedName getImageTwo() {
        return imageTwo;
    }
}
