package org.lpapazov.strongestdota2item.objects;

public class ItemImageWithoutByteArray {
    private String name;
    private String persistedName;
    private double score;
    public long votes;

    public ItemImageWithoutByteArray(String name, String persistedName, double score, long votes) {
        this.name = name;
        this.persistedName = persistedName;
        this.score = score;
        this.votes = votes;
    }

    public String getName() {
        return name;
    }

    public String getPersistedName() {
        return persistedName;
    }

    public double getScore() {
        return score;
    }

    public long getVotes() {
        return votes;
    }
}
