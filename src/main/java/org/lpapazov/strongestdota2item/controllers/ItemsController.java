package org.lpapazov.strongestdota2item.controllers;

import static org.lpapazov.strongestdota2item.EloCalculator.exchangeElos;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.lpapazov.strongestdota2item.RandomImagePairGenerator;
import org.lpapazov.strongestdota2item.entity.ItemImage;
import org.lpapazov.strongestdota2item.objects.ImageNameAndPersistedName;
import org.lpapazov.strongestdota2item.objects.ImagesToCompare;
import org.lpapazov.strongestdota2item.objects.ItemImageWithoutByteArray;
import org.lpapazov.strongestdota2item.objects.StrongerItemVote;
import org.lpapazov.strongestdota2item.repository.ItemImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ItemsController {
    private ItemImageRepository itemImageRepository;
    private List<ImageNameAndPersistedName> allImages = new ArrayList<ImageNameAndPersistedName>();

    public ItemsController(@Autowired ItemImageRepository itemImageRepository) {
        this.itemImageRepository = itemImageRepository;
        Set<ImageNameAndPersistedName> imagesToCompareFromDb = itemImageRepository.findAll()
                .stream()
                .map(itemImage -> new ImageNameAndPersistedName(itemImage.getName(), itemImage.getPersistedName()))
                .collect(Collectors.toSet());
        allImages.addAll(imagesToCompareFromDb);
    }

    @GetMapping("/all-images-count")
    public Integer getAllImagesCount() throws IOException {
        return allImages.size();
    }

    @GetMapping(
            value = "/get-image/{image_persisted_name}",
            produces = MediaType.IMAGE_JPEG_VALUE
    )
    public @ResponseBody byte[] getImageWithMediaType(@PathVariable("image_persisted_name") String imagePersistedName) throws IOException {
        ItemImage itemImage = itemImageRepository.getOne(imagePersistedName);
        return itemImage.getImage();
    }

    @GetMapping("/get-two-random-items")
    public ImagesToCompare getRandomItemsToCompare(@CookieValue(value = "shownImages", required = false) String shownImages) {
        return RandomImagePairGenerator.generate(
                allImages,
                Arrays.stream(
                        Optional.ofNullable(shownImages).orElse("")
                            .split(";"))
                            .filter(e -> !e.equals(""))
                            .collect(Collectors.toList())
        );
    }

    @GetMapping("/image-to-file")
    public String writeImageToFile() throws IOException {
        ItemImage image = itemImageRepository.getOne("Swift blink");

        File outputFile = new File("/Users/l.papazov/BlitzDayProjects/itemscomparator/src/main/resources/img/out.jpg");
        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            outputStream.write(image.getImage());
        }

        return "Saved to file system";
    }

    @PostMapping("/vote-for-item")
    public String writeImageToFile(@RequestBody StrongerItemVote vote) throws IOException {
        ItemImage strongerItem = itemImageRepository.getOne(vote.getStronger());
        ItemImage weakerItem = itemImageRepository.getOne(vote.getWeaker());

        exchangeElos(strongerItem, weakerItem, true);
        strongerItem.incrementVotes();

        itemImageRepository.saveAll(Set.of(strongerItem, weakerItem));

        return "Updated ratings";
    }

    @GetMapping("/save-image")
    public String saveImageInDb() throws IOException {
        Set<String> itemUrls = Set.of(
                "https://liquipedia.net/commons/images/d/d8/Boots_of_Travel.png",
                "https://liquipedia.net/commons/images/7/7c/Bracer.png",
                "https://liquipedia.net/commons/images/a/ab/Falcon_Blade.png",
                "https://liquipedia.net/commons/images/1/1a/Hand_of_Midas.png",
                "https://liquipedia.net/commons/images/6/61/Helm_of_the_Dominator.png",
                "https://liquipedia.net/commons/images/5/5f/Helm_of_the_Overlord.png",
                "https://liquipedia.net/commons/images/b/bc/Magic_Wand.png",
                "https://liquipedia.net/commons/images/3/3e/Mask_of_Madness.png",
                "https://liquipedia.net/commons/images/9/90/Moon_Shard.png",
                "https://liquipedia.net/commons/images/a/a8/Null_Talisman.png",
                "https://liquipedia.net/commons/images/1/1c/Oblivion_Staff.png",
                "https://liquipedia.net/commons/images/d/db/Orb_of_Corrosion.png",
                "https://liquipedia.net/commons/images/7/7f/Perseverance.png",
                "https://liquipedia.net/commons/images/1/1e/Phase_Boots.png",
                "https://liquipedia.net/commons/images/7/73/Power_Treads.png",
                "https://liquipedia.net/commons/images/d/d4/Soul_Ring.png",
                "https://liquipedia.net/commons/images/6/62/Wraith_Band.png",
                "https://liquipedia.net/commons/images/1/15/Arcane_Boots.png",
                "https://liquipedia.net/commons/images/5/58/Boots_of_Bearing.png",
                "https://liquipedia.net/commons/images/7/74/Buckler.png",
                "https://liquipedia.net/commons/images/a/a9/Drum_of_Endurance.png",
                "https://liquipedia.net/commons/images/5/57/Guardian_Greaves.png",
                "https://liquipedia.net/commons/images/e/ea/Headdress.png",
                "https://liquipedia.net/commons/images/e/ec/Holy_Locket.png",
                "https://liquipedia.net/commons/images/6/62/Medallion_of_Courage.png",
                "https://liquipedia.net/commons/images/b/b8/Mekansm.png",
                "https://liquipedia.net/commons/images/1/1e/Pipe_of_Insight.png",
                "https://liquipedia.net/commons/images/a/a4/Ring_of_Basilius.png",
                "https://liquipedia.net/commons/images/9/92/Spirit_Vessel.png",
                "https://liquipedia.net/commons/images/2/22/Tranquil_Boots.png",
                "https://liquipedia.net/commons/images/7/78/Urn_of_Shadows.png",
                "https://liquipedia.net/commons/images/f/f5/Vladmirs_Offering.png",
                "https://liquipedia.net/commons/images/d/d6/Wraith_Pact.png",
                "https://liquipedia.net/commons/images/e/ee/Aether_Lens.png",
                "https://liquipedia.net/commons/images/d/da/Aghanims_Scepter.png",
                "https://liquipedia.net/commons/images/c/c3/Dagon.png",
                "https://liquipedia.net/commons/images/2/2e/Euls_Scepter_of_Divinity.png",
                "https://liquipedia.net/commons/images/9/9f/Force_Staff.png",
                "https://liquipedia.net/commons/images/6/6e/Gleipnir.png",
                "https://liquipedia.net/commons/images/a/ae/Glimmer_Cape.png",
                "https://liquipedia.net/commons/images/2/2e/Octarine_Core.png",
                "https://liquipedia.net/commons/images/a/a2/Refresher_Orb.png",
                "https://liquipedia.net/commons/images/5/55/Rod_of_Atos.png",
                "https://liquipedia.net/commons/images/b/b3/Scythe_of_Vyse.png",
                "https://liquipedia.net/commons/images/5/59/Solar_Crest.png",
                "https://liquipedia.net/commons/images/f/f0/Veil_of_Discord.png",
                "https://liquipedia.net/commons/images/0/06/Wind_Waker.png",
                "https://liquipedia.net/commons/images/f/f9/Witch_Blade.png",
                "https://liquipedia.net/commons/images/0/0a/Aeon_Disk.png",
                "https://liquipedia.net/commons/images/4/40/Assault_Cuirass.png",
                "https://liquipedia.net/commons/images/8/85/Black_King_Bar.png",
                "https://liquipedia.net/commons/images/4/47/Blade_Mail.png",
                "https://liquipedia.net/commons/images/0/00/Bloodstone.png",
                "https://liquipedia.net/commons/images/1/17/Crimson_Guard.png",
                "https://liquipedia.net/commons/images/c/c5/Eternal_Shroud.png",
                "https://liquipedia.net/commons/images/a/a6/Heart_of_Tarrasque.png",
                "https://liquipedia.net/commons/images/b/b3/Hood_of_Defiance.png",
                "https://liquipedia.net/commons/images/7/78/Hurricane_Pike.png",
                "https://liquipedia.net/commons/images/3/3c/Linkens_Sphere.png",
                "https://liquipedia.net/commons/images/b/b1/Lotus_Orb.png",
                "https://liquipedia.net/commons/images/b/b3/Manta_Style.png",
                "https://liquipedia.net/commons/images/8/89/Shivas_Guard.png",
                "https://liquipedia.net/commons/images/a/a5/Soul_Booster.png",
                "https://liquipedia.net/commons/images/f/fd/Vanguard.png",
                "https://liquipedia.net/commons/images/7/70/Abyssal_Blade.png",
                "https://liquipedia.net/commons/images/a/a2/Armlet_of_Mordiggian.png",
                "https://liquipedia.net/commons/images/a/a5/Battle_Fury.png",
                "https://liquipedia.net/commons/images/3/38/Bloodthorn.png",
                "https://liquipedia.net/commons/images/1/10/Butterfly.png",
                "https://liquipedia.net/commons/images/c/c6/Crystalys.png",
                "https://liquipedia.net/commons/images/3/39/Daedalus.png",
                "https://liquipedia.net/commons/images/e/e2/Desolator.png",
                "https://liquipedia.net/commons/images/1/1d/Divine_Rapier.png",
                "https://liquipedia.net/commons/images/c/c9/Ethereal_Blade.png",
                "https://liquipedia.net/commons/images/a/a4/Meteor_Hammer.png",
                "https://liquipedia.net/commons/images/0/0a/Monkey_King_Bar.png",
                "https://liquipedia.net/commons/images/4/41/Nullifier.png",
                "https://liquipedia.net/commons/images/7/71/Radiance.png",
                "https://liquipedia.net/commons/images/7/76/Revenants_Brooch.png",
                "https://liquipedia.net/commons/images/4/4a/Shadow_Blade.png",
                "https://liquipedia.net/commons/images/4/4a/Silver_Edge.png",
                "https://liquipedia.net/commons/images/0/06/Skull_Basher.png",
                "https://liquipedia.net/commons/images/a/a1/Arcane_Blink.png",
                "https://liquipedia.net/commons/images/a/a4/Diffusal_Blade.png",
                "https://liquipedia.net/commons/images/1/18/Dragon_Lance.png",
                "https://liquipedia.net/commons/images/2/26/Echo_Sabre.png",
                "https://liquipedia.net/commons/images/6/64/Eye_of_Skadi.png",
                "https://liquipedia.net/commons/images/0/0a/Heavens_Halberd.png",
                "https://liquipedia.net/commons/images/9/97/Kaya.png",
                "https://liquipedia.net/commons/images/0/0e/Kaya_and_Sange.png",
                "https://liquipedia.net/commons/images/1/14/Maelstrom.png",
                "https://liquipedia.net/commons/images/6/6a/Mage_Slayer.png",
                "https://liquipedia.net/commons/images/b/b7/Mjollnir.png",
                "https://liquipedia.net/commons/images/5/53/Overwhelming_Blink.png",
                "https://liquipedia.net/commons/images/c/ca/Sange.png",
                "https://liquipedia.net/commons/images/1/1c/Sange_and_Yasha.png",
                "https://liquipedia.net/commons/images/0/0e/Satanic.png",
                "https://liquipedia.net/commons/images/0/0e/Swift_Blink.png",
                "https://liquipedia.net/commons/images/e/e8/Yasha.png",
                "https://liquipedia.net/commons/images/0/00/Yasha_and_Kaya.png"
        );

        for (String itemUrl: itemUrls) {
            String[] splitBySlesh = itemUrl.split("/");
            String nameOfImageWithExtension = splitBySlesh[splitBySlesh.length-1];
            String nameOfItem = nameOfImageWithExtension.split("\\.")[0];
            String displayedNameOfItem = nameOfItem.replace("_", " ");

            URL url = new URL(itemUrl);
            byte[] bytesToSaveInDb = url.openStream().readAllBytes();
            ItemImage image = new ItemImage(nameOfItem, displayedNameOfItem, 3278, bytesToSaveInDb, 0);
            itemImageRepository.save(image);
        }

        return "Success";
    }

    @GetMapping("/results")
    public List<ItemImageWithoutByteArray> getResults() {
        return itemImageRepository
                .findAll(Sort.by(Sort.Direction.DESC, "score"))
                .stream()
                .map(itemImage -> new ItemImageWithoutByteArray(itemImage.getName(), itemImage.getPersistedName(), itemImage.getScore(), itemImage.getVotes()))
                .collect(Collectors.toList());
    }

    @PostMapping("/clear-results")
    public void clearResults(@RequestBody String key) {
        if (System.getProperty("key").equals(key)) {
            itemImageRepository
                    .findAll()
                    .forEach(image -> {
                        image.setScore(3278);
                        image.setVotes(0);
                        itemImageRepository.save(image);
                    });
        };
    }
}
