package org.lpapazov.strongestdota2item.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
//Just makes sure these endpoints hit the frontend, React takes care of the actual routing
public class FrontEndController {
    @RequestMapping(value = "/")
    public String index() {
        return "index.html";
    }

    @RequestMapping(value = "/results")
    public String results() {
        return "index.html";
    }
}
