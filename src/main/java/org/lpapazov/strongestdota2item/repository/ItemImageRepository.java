package org.lpapazov.strongestdota2item.repository;

import org.lpapazov.strongestdota2item.entity.ItemImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemImageRepository extends JpaRepository<ItemImage, String> {

}
