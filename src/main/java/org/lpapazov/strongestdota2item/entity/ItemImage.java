package org.lpapazov.strongestdota2item.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "image")
public class ItemImage {

    @Id
    @Column(name = "persisted_name")
    private String persistedName;

    @Column
    private String name;

    @Column(name = "votes_count")
    private long votes;

    @Column
    private double score;

    public void setScore(double score) {
        this.score = score;
    }

    public void setVotes(long votes) {
        this.votes = votes;
    }

    public String getName() {
        return name;
    }

    public String getPersistedName() {
        return persistedName;
    }

    public double getScore() {
        return score;
    }

    public byte[] getImage() {
        return image;
    }

    public long getVotes() {
        return votes;
    }

    public void incrementVotes() {
        votes = votes + 1;
    }

    @Column
    private byte[] image;

    public ItemImage(String persistedName, String name, double score, byte[] image, int votes) {
        this.votes = votes;
        this.persistedName = persistedName;
        this.name = name;
        this.score = score;
        this.image = image;
    }

    public ItemImage() {
    }
}
