package org.lpapazov.strongestdota2item;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lpapazov.strongestdota2item.objects.ImageNameAndPersistedName;
import org.lpapazov.strongestdota2item.objects.ImagesToCompare;

public class RandomImagePairGenerator {
    private static final Random RAND = new Random();

    public static ImagesToCompare generate(List<ImageNameAndPersistedName> allImages, List<String> excluded) {
        List<ImageNameAndPersistedName> unusedImages = new ArrayList<>(allImages);
        if (allImages.size() - excluded.size() >= 2) {
            unusedImages.removeIf(imageNameAndPersistedName -> excluded.contains(imageNameAndPersistedName.getPersistedName()));
        }

        ImageNameAndPersistedName image1 = unusedImages.get(RAND.nextInt(unusedImages.size()));
        ImageNameAndPersistedName image2;
        do {
            image2 = unusedImages.get(RAND.nextInt(unusedImages.size()));
        } while (image1.equals(image2));

        return new ImagesToCompare(image1, image2);
    }
}
