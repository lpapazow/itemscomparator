package org.lpapazov.strongestdota2item;

import org.lpapazov.strongestdota2item.entity.ItemImage;

public class EloCalculator {

    private static final double CORRELATION_BTW_EXPECTATION_AND_CHANGE = 2000;
    private static final double ALGORITHM_SENSITIVITY = 250;

    public static void exchangeElos(ItemImage a, ItemImage b, boolean itemAWasVotedAsStronger) {

        double expectationA = 1 / (1 + Math.pow(10, (b.getScore() - a.getScore()) / CORRELATION_BTW_EXPECTATION_AND_CHANGE));
        double expectationB = 1 / (1 + Math.pow(10, (a.getScore() - b.getScore()) / CORRELATION_BTW_EXPECTATION_AND_CHANGE));

        double newRatingA = a.getScore() + ALGORITHM_SENSITIVITY * ((itemAWasVotedAsStronger ? 1 : 0) - expectationA);
        double newRatingB = b.getScore() + ALGORITHM_SENSITIVITY * ((itemAWasVotedAsStronger ? 0 : 1) - expectationB);

        a.setScore(newRatingA);
        b.setScore(newRatingB);
    }

}
