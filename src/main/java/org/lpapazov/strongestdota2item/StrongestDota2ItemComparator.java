package org.lpapazov.strongestdota2item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrongestDota2ItemComparator {
	public static void main(String[] args) {
		SpringApplication.run(StrongestDota2ItemComparator.class, args);
	}
}
